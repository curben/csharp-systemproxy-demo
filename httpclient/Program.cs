static partial class Program
{
    // <sharedclient>
    // HttpClient lifecycle management best practices:
    // https://learn.microsoft.com/dotnet/fundamentals/networking/http/httpclient-guidelines#recommended-use
    private static HttpClient sharedClient = new()
    {
        BaseAddress = new Uri("http://example.com"),
    };
    // </sharedclient>
    public static async Task Main(string[] args)
    {
        await GetAsync(sharedClient);

        // <client>
        // https://stackoverflow.com/a/76189253
        HttpClientHandler handler = new HttpClientHandler();
        IWebProxy proxy = WebRequest.GetSystemWebProxy();
        proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
        handler.UseDefaultCredentials = true;
        handler.Proxy = proxy;

        using var httpClient = new HttpClient(handler);
        // </client>
    }
}
