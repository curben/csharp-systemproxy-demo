static partial class Program
{
    // <get>
    static async Task GetAsync(HttpClient httpClient)
    {
        using HttpResponseMessage response = await httpClient.GetAsync("");

        response.EnsureSuccessStatusCode()
            .WriteRequestToConsole();

        var bodyResponse = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"{bodyResponse}\n");

        // Expected output:
        //   GET http://example.com/ HTTP/1.1
        //   <!doctype html>
        //   ...
    }
    // </get>
}
