# Using C# HTTP client with system proxy

- [httpclient](./httpclient/): C# HTTP client based on [donet/docs](https://github.com/dotnet/docs/tree/main/docs/fundamentals/networking/snippets/httpclient)
  - It sends a GET request to http://example.com and shows the response body.
- [nginx](./nginx/): Configure NGINX as a HTTP (forward) proxy.
  - Listens on http://localhost:8888
  - Serves [proxy.pac](./nginx/html/proxy.pac) on http://localhost:8888/proxy.pac
    - Only example.com will go through the proxy.

## Windows instruction

1. Download and unzip [nginx](https://nginx.org/en/docs/windows.html).
2. Replace [conf/nginx.conf](./nginx/conf/nginx.conf).
3. Copy [html/proxy.pac](./nginx/html/proxy.pac)
4. `start nginx`
5. `curl.exe http://localhost:8888/proxy.pac`
6. logs/access.log should have `"GET /proxy.pac HTTP/1.1" 200` line.
7. Windows Settings -> Network & Internet -> Proxy.
   1. Untick "Automatically detect settings"
   2. Edit "Use setup script"
      - Tick "Use setup script"
      - Enter http://localhost:8888/proxy.pac
8. Build and run [httpclient](./httpclient/)
   - Tested on .NET SDK 8
9. logs/access.log should have `"GET http://example.com/ HTTP/1.1" 200` line, without user agent value.
